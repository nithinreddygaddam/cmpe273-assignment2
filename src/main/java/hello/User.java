package hello;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

public class User 
{
	private String id;
	private String name;		//optional
	private String created_at;  //change type to time
	private String updated_at;
	
	@NotBlank(message="Email is empty")
	private String email;
	
	@NotBlank(message="Password is empty")
	private String password;
	
	public User(){}
	
	public User(String email, String password,String created_at) 
	{
		super();
		//userID_counter++;
		this.email = email;
		this.password = password;
		this.created_at = created_at;
	}

	public String getUserID() 
	{
		return id;
	}
	
	public void setUserID(String user_id) 
	{
		this.id = user_id;
	}
	
	public String getEmail() 
	{
		return email;
	}
	
	public void setEmail(String email) 
	{
		this.email = email;
	}
	
	public String getPassword() 
	{
		return password;
	}
	
	public void setPassword(String password) 
	{
		this.password = password;
	}
	
	public String getCreatedAt() 
	{
		return created_at;
	}
	
	public void setCreatedAt(String created_at) 
	{
		this.created_at = created_at;
	}
	
	public String getUpdatedAt() 
	{
		return updated_at;
	}
	
	public void setUpdatedAt(String updated_at) 
	{
		this.updated_at = updated_at;
	}
	
}
