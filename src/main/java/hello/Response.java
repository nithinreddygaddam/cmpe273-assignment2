package hello;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {
	private String change_date;
	private String data_view_code;
	private String customer_name;
	private String message;
	private String record_type_code;
	private String zip;
	private String office_code;
	private String telephone;
	private String routing_number;
	private String rn;
	private String address;
	private String code;
	private String state;
	private String new_routing_number;
	private String institution_status_code;
	private String city;
	
	public String getCustomer_name(){
		return customer_name;
	}
	
	public String getCode(){
		return code;
	}
}
