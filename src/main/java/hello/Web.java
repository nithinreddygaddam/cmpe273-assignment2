package hello;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;

public class Web 
{
	private String id;
	private String user_id;

	@NotBlank(message ="Empty URL")
	private String url;
	
	@NotBlank(message ="Empty Login")
	private String login;
	
	@NotBlank(message ="Empty Password")
	private String password;

	public Web(String url, String login, String password, String user_id) 
	{
		super();
		this.url = url;
		this.login = login;
		this.password = password;
		this.user_id = user_id;
	}
	
	public Web(){}
	
	public String getUser_id() 
	{
		return user_id;
	}
	
	public void setUser_id(String user_id) 
	{
		this.user_id = user_id;
	}
	
	public String getLogin_id() 
	{
		return id;
	}
	
	public void setLogin_id(String login_id) 
	{
		this.id = login_id;
	}
	
	public String getUrl() 
	{
		return url;
	}
	
	public void setUrl(String url) 
	{
		this.url = url;
	}
	
	public String getLogin()
	{
		return login;
	}
	
	public void setLogin(String login) 
	{
		this.login = login;
	}
	
	public String getPassword() 
	{
		return password;
	}
	
	public void setPassword(String password) 
	{
		this.password = password;
	}	

}
