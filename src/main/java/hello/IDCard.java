package hello;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;

public class IDCard
{

	private String id;
	private String expiration_date;		//optional
	
	@NotBlank(message = "No Card number")
	private String card_number;
	
	@NotBlank(message = "No Card name")
	private String card_name;
	private String user_id;

	public IDCard(){}
	
	public IDCard(String card_name, String card_number,String expiration_date, String user_id) 
	{
		super();
		this.card_number = card_number;
		this.card_name = card_name;
		this.expiration_date = expiration_date;
		this.user_id = user_id;
	}
	
	public String getCard_id() 
	{
		return id;
	}
	
	public void setCard_id(String card_id) 
	{
		this.id = card_id;
	}
	
	public String getUser_id() 
	{
		return user_id;
	}
	
	public void setUser_id(String user_id) 
	{
		this.user_id = user_id;
	}
	
	public String getCard_number() 
	{
		return card_number;
	}
	
	public void setCard_number(String card_number) 
	{
		this.card_number = card_number;
	}
	
	public String getCard_name() 
	{
		return card_name;
	}
	
	public void setCard_name(String card_name) 
	{
		this.card_name = card_name;
	}
	
	public String getExpiration_date() 
	{
		return expiration_date;
	}
	
	public void setExpiration_date(String expiration_date) 
	{
		this.expiration_date = expiration_date;
	}
	
	
}
