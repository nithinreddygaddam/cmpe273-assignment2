package hello;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public class SpringMongoConfig {
	
	public @Bean
	MongoTemplate mongoTemplate() throws Exception {
		
		String textUri = "mongodb://nithin:test@ds049160.mongolab.com:49160/walletdb";
        MongoClientURI uri = new MongoClientURI(textUri);
		MongoTemplate mongoTemplate = new MongoTemplate(new MongoClient(uri), "walletdb");
		return mongoTemplate;
		
	}

}
