package hello;

import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;

import java.util.Date;

import org.springframework.context.ApplicationContext;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.util.List;
import java.util.ArrayList;

@RestController
public class Controller 
{

	ApplicationContext ctx = 
			new AnnotationConfigApplicationContext(SpringMongoConfig.class);
		MongoOperations mongoOperation = 
			(MongoOperations) ctx.getBean("mongoTemplate");
	
	Date date = new Date();
	
//POST User
	@RequestMapping(value = "api/v1/users", method = RequestMethod.POST)
	@ResponseStatus(org.springframework.http.HttpStatus.CREATED)
    public User createUser(@Valid @RequestBody User user) 
	{
		User new_User  = new User(user.getEmail(),user.getPassword(),date.toString());;
		mongoOperation.save(new_User, "User");
		return new_User;
    }
	
//GET User
	@RequestMapping(value = "api/v1/users/{user_id}", method = RequestMethod.GET)
	@ResponseStatus(org.springframework.http.HttpStatus.OK)
    public User viewUser(@PathVariable(value = "user_id") String user_id) 
	{
		Query findUserQuery = new Query();
		findUserQuery.addCriteria(Criteria.where("id").is(user_id));
		User user1 = mongoOperation.findOne(findUserQuery, User.class, "User");
		return user1;
	}
	
//PUT User
	@RequestMapping(value = "api/v1/users/{user_id}", method = RequestMethod.PUT)
	@ResponseStatus(org.springframework.http.HttpStatus.CREATED)
    public User updateUser(@Valid @RequestBody User user,@PathVariable(value = "user_id") String user_id) 
	{

		Query findUserQuery = new Query();
		findUserQuery.addCriteria(Criteria.where("id").is(user_id));
		User current_user = mongoOperation.findOne(findUserQuery, User.class, "User");
		
		if ( current_user != null )
		{
			current_user.setPassword(user.getPassword());
			current_user.setEmail(user.getEmail());
			current_user.setUpdatedAt(date.toString());
			mongoOperation.save(current_user, "User");
		}
		
		return current_user;
    }

// POST ID card	
	@RequestMapping(value = "api/v1/users/{user_id}/idcards", method = RequestMethod.POST)
	@ResponseStatus(org.springframework.http.HttpStatus.CREATED)
    public IDCard createIDCard(@Valid @RequestBody IDCard id,@PathVariable(value = "user_id") String user_id) 
	{	
		IDCard newID = new IDCard(id.getCard_name(),id.getCard_number(),id.getExpiration_date(), user_id);
		mongoOperation.save(newID, "ID");
		return newID;
	}
	
//GET ID card	
	@RequestMapping(value = "api/v1/users/{user_id}/idcards", method = RequestMethod.GET)
	@ResponseStatus(org.springframework.http.HttpStatus.OK)
    public List<IDCard> viewIDCards(@PathVariable(value = "user_id") String user_id) 
	{
		Query findIDQuery = new Query();
		findIDQuery.addCriteria(Criteria.where("user_id").is(user_id));
		List<IDCard> idCards =  mongoOperation.find(findIDQuery, IDCard.class, "ID");
		return idCards;
	}
	
//DELETE ID Card	
	@RequestMapping(value = "api/v1/users/{user_id}/idcards/{card_id}", method = RequestMethod.DELETE)
	@ResponseStatus(org.springframework.http.HttpStatus.NO_CONTENT)
    public void deleteIDCard(@PathVariable(value = "user_id") String user_id,@PathVariable(value = "card_id") String card_id) 
	{	
		Query deleteIDQuery = new Query();
		deleteIDQuery.addCriteria(Criteria.where("id").is(card_id));
		deleteIDQuery.addCriteria(Criteria.where("user_id").is(user_id));
		mongoOperation.remove(deleteIDQuery, IDCard.class, "ID");
	}
	
//POST Web Login
	@RequestMapping(value = "api/v1/users/{user_id}/weblogins", method = RequestMethod.POST)
	@ResponseStatus(org.springframework.http.HttpStatus.CREATED)
    public Web createWebID(@Valid @RequestBody Web id,@PathVariable(value = "user_id") String user_id) 
	{	
		Web newWeb = new Web(id.getUrl(),id.getLogin(),id.getPassword(), user_id);
		mongoOperation.save(newWeb, "Web");
		return newWeb;
	}
	
//GET Web Login	
	@RequestMapping(value = "api/v1/users/{user_id}/weblogins", method = RequestMethod.GET)
	@ResponseStatus(org.springframework.http.HttpStatus.OK)
    public List<Web> viewWebs(@PathVariable(value = "user_id") String user_id) 
	{
		Query findWebQuery = new Query();
		findWebQuery.addCriteria(Criteria.where("user_id").is(user_id));
		return mongoOperation.find(findWebQuery, Web.class, "Web");

	}
	
//DELETE Web Login	
	@RequestMapping(value = "api/v1/users/{user_id}/weblogins/{login_id}", method = RequestMethod.DELETE)
	@ResponseStatus(org.springframework.http.HttpStatus.NO_CONTENT)
    public void deleteWeb(@PathVariable(value = "user_id") String user_id,@PathVariable(value = "login_id") String login_id) 
	{
		Query deleteWebQuery = new Query();
		deleteWebQuery.addCriteria(Criteria.where("id").is(login_id));
		deleteWebQuery.addCriteria(Criteria.where("user_id").is(user_id));
		mongoOperation.remove(deleteWebQuery, Web.class, "Web");
	}
	
		
//POST Bank Account	
	@RequestMapping(value = "api/v1/users/{user_id}/bankaccounts", method = RequestMethod.POST)
	@ResponseStatus(org.springframework.http.HttpStatus.CREATED)
    public Account createAccount(@Valid @RequestBody Account bank,@PathVariable(value = "user_id") String user_id) throws ParseException, UnirestException 
	{	
		Account newAccount = new Account(bank.getAccount_name(),bank.getAccount_number(),bank.getRouting_number(), user_id);
		
		String url = "http://www.routingnumbers.info/api/data.json?rn=" + newAccount.getRouting_number();
		
		// Create a new RestTemplate instance
		HttpResponse<JsonNode> response = Unirest.get(url).asJson();
		JsonNode body = response.getBody();
		if(String.valueOf(body.getObject().get("code")).equals("200"))
		{
			newAccount.setAccountName(String.valueOf(body.getObject().get("customer_name")));
			mongoOperation.save(newAccount, "Bank");	
		}
		else
		{
			newAccount = null;
		}
		
		return newAccount;
	}

//GET Bank Account
	@RequestMapping(value = "api/v1/users/{user_id}/bankaccounts", method = RequestMethod.GET)
	@ResponseStatus(org.springframework.http.HttpStatus.OK)
    public List<Account> viewAccounts(@PathVariable(value="user_id") String user_id) 
	{
		Query findBankQuery = new Query();
		findBankQuery.addCriteria(Criteria.where("user_id").is(user_id));
		return mongoOperation.find(findBankQuery, Account.class, "Bank");
	} 

//DELETE Bank Account	
	@RequestMapping(value = "api/v1/users/{user_id}/bankaccounts/{ba_id}", method = RequestMethod.DELETE)
	@ResponseStatus(org.springframework.http.HttpStatus.NO_CONTENT)
    public void deleteAccount(@PathVariable(value = "user_id") String user_id,@PathVariable(value = "ba_id") String ba_id) 
    {
		Query deleteAccountQuery = new Query();
		deleteAccountQuery.addCriteria(Criteria.where("id").is(ba_id));
		deleteAccountQuery.addCriteria(Criteria.where("user_id").is(user_id));
		mongoOperation.remove(deleteAccountQuery, Account.class, "Bank");
	}
	
//Exception Handler
	@ExceptionHandler
    @ResponseStatus(org.springframework.http.HttpStatus.BAD_REQUEST)
    @ResponseBody
    ExceptionHandlers handleException(Exception ex) {
		System.out.println("in exception handler");
        List<FieldError> fieldErrors = ((MethodArgumentNotValidException) ex).getBindingResult().getFieldErrors();
        List<ObjectError> globalErrors = ((MethodArgumentNotValidException) ex).getBindingResult().getGlobalErrors();
        List<String> errors = new ArrayList<String>(fieldErrors.size()+globalErrors.size());
        String error;
        for (FieldError fieldError : fieldErrors) {
            error = fieldError.getField() + ", " + fieldError.getDefaultMessage();
            errors.add(error);
        }
        for (ObjectError objectError : globalErrors) {
            error = objectError.getObjectName() + ", " + objectError.getDefaultMessage();
            errors.add(error);
        }
        return new ExceptionHandlers(errors);
    } 
	}
