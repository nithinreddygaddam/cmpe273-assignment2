package hello;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;

public class Account 
{
	
	private String id;
	private String account_name;
	private String user_id;
	
	
	@NotBlank(message = "Account number is empty")
	private String account_number;
	
	@NotBlank(message = "Routing number is empty")
	private String routing_number;
	

	private Account(){}
	
	public Account(String account_name,String account_number, String routing_number, String user_id) 
	{
		super();
		
		this.account_name = account_name;
		this.account_number = account_number;
		this.routing_number = routing_number;
		this.user_id = user_id;
	}
		
	public String getAccount_id() 
	{
		return id;
	}
	
	public void setAccount_id(String account_id) 
	{
		this.id = account_id;
	}
	
	public String getAccount_name() 
	{
		return account_name;
	}
	
	public void setAccountName(String account_name) 
	{
		this.account_name = account_name;
	}
	
	public String getAccount_number() 
	{
		return account_number;
	}
	
	public void setAccount_number(String account_number) 
	{
		this.account_number = account_number;
	}
	
	public String getRouting_number() 
	{
		return routing_number;
	}
	
	public void setRouting_number(String routing_number) 
	{
		this.routing_number = routing_number;
	}

	public String getUser_id() 
	{
		return user_id;
	}
	
	public void setUser_id(String user_id) 
	{
		this.user_id = user_id;
	}
	
}
